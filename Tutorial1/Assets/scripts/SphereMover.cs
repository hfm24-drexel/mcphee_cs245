﻿using UnityEngine;
using System.Collections;

public class SphereMover : MonoBehaviour {

	public float speed;
	public float jumpForce;

	private Rigidbody _myRigidBody;

	// Use this for initialization
	void Start () {
		_myRigidBody = transform.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.UpArrow))
			_myRigidBody.velocity = new Vector3(0f, _myRigidBody.velocity.y, speed);
		if (Input.GetKey (KeyCode.DownArrow))
			_myRigidBody.velocity = new Vector3(0f, _myRigidBody.velocity.y, -speed);
		if (Input.GetKey (KeyCode.LeftArrow))
			_myRigidBody.velocity = new Vector3(-speed, _myRigidBody.velocity.y, 0f);
		if (Input.GetKey (KeyCode.RightArrow))
			_myRigidBody.velocity = new Vector3(speed, _myRigidBody.velocity.y, 0f);
		
		if (Input.GetKeyDown (KeyCode.Space) && IsOnGround())
			_myRigidBody.velocity = new Vector3(_myRigidBody.velocity.x, jumpForce, _myRigidBody.velocity.z);
	}

	private bool IsOnGround() {
		return Physics.Raycast (transform.position, Vector3.down, 0.51f);
	}
}
